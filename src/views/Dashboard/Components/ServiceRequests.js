import { element } from "prop-types";
import React, { Component } from "react";
import { Card, Row } from "react-bootstrap";
import {CgShapeCircle} from 'react-icons/cg';
import DashboardService from "../Services/DashboardService";

function ServiceRequests(props) {
  var vijayawada ;

  DashboardService.getCities().then(response => {
    console.log("respo", response);
    vijayawada = response.data.vijayawada;
  
    

  const data =[
    {
      place:"Vijayawada",
      requests:vijayawada,
      color:"red"
    },
    {
      place:"Hyderabad",
      requests:80,
      color:"purple"
    },
    {
      place:"Visakhapatnam",
      requests:10,
      color:"blue"
    },
    {
      place:"Guntur",
      requests:3,
      color:"green"
    },
    {
      place:"Rajahmundry",
      requests:5,
      color:"orange"
    },
  ]
  return (
    <div className="col-lg-4 mb-4 col-sm-6 col-md-6 col-xs-12">
     <div className="card" style={{padding:20,height:450,width:500,alignContent:"center"}}>
     <div className="card-body">
        
        <div className="stat-heading" style={{textAlign:'left',marginBottom:20,fontSize:30}}>Service Requests</div>
   <hr />
            <div className="item1">
                <div style={{float:"left",fontSize:20}}>
                 {
                   data.map((element,index)=>{
                     return <div key={index} className="item-title">
                       <Card style={{display:"table-row",width:250,height:50,padding:100}} >
                       < CgShapeCircle size={30} color={element.color} />
                       {element.place}
                       </Card>
                       </div>
                   })
                 }
                </div>
                <i
                className={`fa fa-${props.icon1} ${props.color1} fa-2x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {
                   data.map((element,index)=>{
                     return <div key={index} >
                       <div style={{display:"table-row",width:250,height:50,padding:100,}} >
                       {element.requests}
                       </div>
                       </div>
                   })
                 }
              </i>
            </div>
           
           
            
           
            
            
           
            
         
        </div>
      </div>
    </div>
  );
});
}

export default ServiceRequests;