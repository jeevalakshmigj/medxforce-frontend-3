
import React, { Component } from "react";
import "./StyleSheets/ServiceCard.css";
import PropTypes from "prop-types";
import classNames from "classnames";

function ServiceCard(props) {
  return (
    <div
      href="#"
      className={classNames("service--card", props.className)}
      onClick={props.onClick}
    >
      <div class="overlay"></div>
      <div >{props.icon}</div>
      <p>Service:</p>
      <div className="stat-heading" style={{textAlign:'left',marginBottom:20,fontSize:30}}>{props.title1}</div>
      <div className="item1">
                <div className="item-title" style={{float:"left"}}>
                Active
                </div>
                <i
                className={`fa fa-${props.icon1} ${props.color1} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count1}
              </i>
      </div>

      <br />
            <br />
            <hr />
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
                Inactive
                </div>
                <i
                className={`fa fa-${props.icon2} ${props.color2} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              2
              </i>
            </div>
    </div>
  );
}

ServiceCard.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.any,
  className: PropTypes.string,
  onClick: PropTypes.func
};

// Specifies the default values for props:
ServiceCard.defaultProps = {
  title: "Profile",
  icon: <i class="fa fa-google-wallet fa-3x" aria-hidden="true"></i>,
  className: "default"
};
export default ServiceCard;
